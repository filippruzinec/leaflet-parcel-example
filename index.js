import { map as createLeafletMap } from "leaflet";
import "leaflet/dist/leaflet.css";
import { mapSettings } from "./common/settings";
import { layers } from "./layer";
import "./style.css";

const map = createLeafletMap("map").setView(
  [mapSettings.view.lat, mapSettings.view.lng],
  mapSettings.zoom
);

map.addLayer(layers.OSMLayer);
